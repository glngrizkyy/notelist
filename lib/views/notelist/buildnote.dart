// ignore_for_file: prefer_const_constructors_in_immutables, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:product/db/database.dart';
import 'package:product/note/note.dart';
import 'package:product/views/notelist/notedetail.dart';
import 'package:product/widget/notecardwidget.dart';

class BuildNote extends StatefulWidget {
  BuildNote({Key? key}) : super(key: key);

  @override
  State<BuildNote> createState() => _BuildNoteState();
}

class _BuildNoteState extends State<BuildNote> {
  late List<Note> notes;

  bool isLoading = false;

  @override
  void initState() {
    super.initState();

    refreshNotes();
  }

  @override
  void dispose() {
    NotesDatabase.instance.close();

    super.dispose();
  }

  Future refreshNotes() async {
    setState(() => isLoading = true);

    notes = await NotesDatabase.instance.readAllNotes();

    setState(() => isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: notes.length,
      itemBuilder: (context, index) {
        return StaggeredGridView.countBuilder(
          padding: EdgeInsets.all(8),
          itemCount: notes.length,
          staggeredTileBuilder: (index) => StaggeredTile.fit(2),
          crossAxisCount: 4,
          mainAxisSpacing: 4,
          crossAxisSpacing: 4,
          itemBuilder: (context, index) {
            final note = notes[index];

            return GestureDetector(
              onTap: () async {
                await Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => NoteDetailPage(noteId: note.id!),
                ));

                refreshNotes();
              },
              child: NoteCardWidget(note: note, index: index),
            );
          },
        );
      },
    );
  }
}

// Center(
//         child: isLoading
//             ? CircularProgressIndicator()
//             : notes.isEmpty
//                 ? Text(
//                     'No Notes',
//                     style: TextStyle(color: backgroundColor6, fontSize: 24),
//                   )
//                 : buildNotes(),
//       ),
//       floatingActionButton: FloatingActionButton(
//         backgroundColor: greenColor,
//         child: Icon(
//           Icons.add,
//           color: backgroundColor6,
//         ),
//         onPressed: () async {
//           await Navigator.of(context).push(
//             MaterialPageRoute(builder: (context) => AddEditNotePage()),
//           );

//           refreshNotes();
//         },
//       ),
//     );
//   }

//   Widget buildNotes() => StaggeredGridView.countBuilder(
//         padding: EdgeInsets.all(8),
//         itemCount: notes.length,
//         staggeredTileBuilder: (index) => StaggeredTile.fit(2),
//         crossAxisCount: 4,
//         mainAxisSpacing: 4,
//         crossAxisSpacing: 4,
//         itemBuilder: (context, index) {
//           final note = notes[index];

//           return GestureDetector(
//             onTap: () async {
//               await Navigator.of(context).push(MaterialPageRoute(
//                 builder: (context) => NoteDetailPage(noteId: note.id!),
//               ));

//               refreshNotes();
//             },
//             child: NoteCardWidget(note: note, index: index),
//           );
//         },